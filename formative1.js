/*
* Formative Day 12
* Bayu Seno Ariefyanto
*/
/* 
Problem

Buatkan sebuah aplikasi sederhana untuk menghitung 
biaya warnet selama mengerjakan tugasdari seseorang 
bernama toni.

Dari tiap tugas coba buatkan fungsi
yang mengkonversi durasi jam. Lalu dari durasi jam
yang di dapat kamu filter mana tugas yang membutuhkan
waktu pengerjaan dua jam atau lebih. Setelah memfilter
coba kalikan hasilnya dengan tarif per jam ( 25 Dollar ) 
untuk penagihan. Lalu cetak outputnya menjadi berformat 
dollar currency.

*/

var day1 = [
    {
        'name': 'Harvest Moon Back To Nature',
        'duration': 180
    },
    {
        'name': 'How Become Developer?',
        'duration': 120
    }
];

var day2 = [
    {
        'name': 'Web Development Kit',
        'duration': 240
    },
    {
        'name': 'Become Rich Only 1 Month',
        'duration': 180
    },
    {
        'name': 'A Whole New World',
        'duration': 240
    }
];

var tasks = [day1, day2];

const calculateTotalTaskPrice = (taskOfdays) => taskOfdays.reduce((accumulator, current) => {
    let price = accumulator;
    for (const task of current) {
        if (task.duration / 60 >= 2) {
            price += task.duration / 60 * 25
        }
    }
    return accumulator = price;
}, 0);

const numberFormat = Intl.NumberFormat("en-US", {
    style: 'currency', currency: 'USD'
});

console.log(numberFormat.format(calculateTotalTaskPrice(tasks)));
